using System.Runtime.InteropServices;
using System;
using Microsoft.EntityFrameworkCore;

namespace WebApplication8
{
    public class AirBusDbContext : DbContext
    {
        public DbSet<AirBus> AriBuses { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=LAPTOP-R91MV32G;Database=AirBusDb;Trusted_Connection=True;");
            base.OnConfiguring(optionsBuilder);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AirBus>(x =>
            {
                x.HasKey(x => x.Id);
                x.Property(x => x.Id).ValueGeneratedOnAdd(); 
            });
            base.OnModelCreating(modelBuilder);
        }

    }
}
