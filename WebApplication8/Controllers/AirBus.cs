using System.Runtime.InteropServices;
using System;

namespace WebApplication8
{
    public class AirBus
    {
        public int Id { get; set; }
        public int Length { get; set; }
        public string Name { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public int PassengersCount { get; set; }  
    }
}